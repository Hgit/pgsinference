""" find hierarchies using only properties """

## import
import itertools
import copy
import json 
import re
import ast # to convert string to dict

from regraph import Neo4jGraph

from format_utils import label_format

###############################"
def get_list_content(prop):
    """ Get the content of the lists contained in the string prop.
    
    Parameters
    ----------
    prop : str
    
    Returns
    -------
    proplists : Python list
        Content of lists contained in prop
    propelems : Python list
        List of the elements in prop (except list content) formatted adequately
    """
    
    proplists = [] # content of lists contained in prop
    propelems = [] # list of the elements in prop1 formatted adequately
    
    indexBracketl = prop.find("[") # index of "["
    indexBracketr = prop.rfind("]") # index of "]"
    if indexBracketl > -1:
        # there is a list in the string prop1
        proplists.append(prop[indexBracketl+1 : indexBracketr].strip("'"))
        propNotlist = prop[: indexBracketl] + prop[indexBracketr+1 : ] # not inside the list
        propelems += list(filter(lambda x: x != "", propNotlist.split(' + ')))
    else:
        # there is no lists  in the string
        propelems += prop.split(" + ")
        
    return proplists, propelems

def get_dict_content(prop):
    """ Get the content of the dict contained in the string prop.
    
    Parameters
    ----------
    prop : str
    
    Returns
    -------
    propdicts : Python list
        Content of dicts contained in prop
    propelems : Python list
        List of the elements in prop (except dict content) formatted adequately
    """
    
    propdicts = [] # content of dicts contained in prop
    propelems = [] # list of the elements in prop1 formatted adequately
    
    indexBracketl = prop.find("{") # index of "{"
    indexBracketr = prop.rfind("}") # index of "}"
    if indexBracketl > -1:
        # there is a dict in the string prop
        dicts = prop[indexBracketl : indexBracketr+1].strip("'")
        if "+" in prop[indexBracketl : indexBracketr+1].strip("'"):
            dictList = dicts.split(" + ")
            dictelem = dictList[0]
            for x in dictList[1:]:
                dicts = merge_data_types(dictelem,x)
        propdicts.append(ast.literal_eval(dicts))
        propNotdict = prop[: indexBracketl] + prop[indexBracketr+1 : ] # not inside the dict
        propelems += list(filter(lambda x: x != "", propNotdict.split(' + ')))
        
    else:
        # there is no dict  in the string
        propelems += prop.split(" + ")
        
    return propdicts, propelems
    

def merge_data_types(prop1, prop2):
    """ Merges two data types: prop1 and prop2.
    
    Parameters
    ----------
    prop1 : dict, Python list or string
        A property value data type.
        The dict values can be dicts, lists or strings.
        The list can contain dicts, lists or strings.
    prop2 : dict, Python list or string
        A property value data type.
        The dict values can be dicts, lists or strings.
        The list can contain dicts, lists or strings.
        
    Returns
    -------
    propMerged : dict, Python list or string
        The merged data type.
        
    """
    if type(prop1) != type(prop2):
        # then, the merged data type is a union of the two data types
        sep = " + " # separator for the union of data types
        propMerged = sep.join({str(prop1), str(prop2)})
        
    elif type(prop1) == list:
        # then, prop2 is also a list and the merged data type is a list
        propMerged = ""
                
        prop1dicts = [] # list of dictionnaries contained in prop1
        prop2dicts = [] # list of dictionnaries contained in prop2
        
        prop1lists = [] # content of lists contained in prop1
        prop2lists = [] # content of lists contained in prop2
        
        prop1elems = [] # list of the elements in prop1 formatted adequately
        for elem in prop1:
            if type(elem) == dict:
                prop1dicts.append(elem)
                
            elif type(elem) == list:
                prop1lists += elem
            else:
                prop1lst, prop1elems = get_list_content(elem)
                prop1lists += prop1lst
                
        prop2elems = [] # list of the elements in prop2 formatted adequately
        for elem in prop2:
            if type(elem) == dict:
                prop2dicts.append(elem)
            elif type(elem) == list:
                prop2lists += elem
            else:
                prop2lst, prop2elems = get_list_content(elem)
                prop2lists += prop2lst
        
        ## deal with potential lists        
        proplistSet = set()
        if prop1lists and prop2lists:
            # both prop1lists and prop2lists are non-empty (i.e., they contain lists)
            for pair in list(itertools.product(prop1lists, prop2lists)):
                mergedPair = merge_data_types(pair[0], pair[1]) # recursive call
                proplistSet.update(set(mergedPair.split(" + ")))
                
            proplistSet = {str([" + ".join(proplistSet)])}
        else:
            # at least one of the two is empty
            if prop1lists:
                # prop1list is not empty
                prop1elems.append(str(prop1lists)) 
            elif prop2lists:
                # prop2list is not empty
                prop2elems.append(str(prop2lists))
                    
        # deal with the dicts:
        propdictList = []
        if prop1dicts and prop2dicts:
            # none of the two is empty
            for pair in list(itertools.product(prop1dicts, prop2dicts)):
                mergedPair = merge_data_types(pair[0], pair[1]) # recursive call
                propdictList.append(json.dumps(mergedPair))
                  
        else:
            propdictList += list(map(json.dumps, prop1dicts))
            propdictList += list(map(json.dumps, prop2dicts))
        
        propMerged = " + ".join(set(prop1elems) | set(prop2elems) | set(propdictList) | proplistSet)
        
        # wrap mergedPair in a list
        propMerged = str([propMerged])
        
        propMerged = [merge_data_types(prop1[0], prop2[0])]
        
        
    elif type(prop1) == dict:
        # then, prop2 is also a dict and the merged data type is a dict
        propMerged = {}
        # property keys intersection:
        propKeyInters = set(prop1.keys()) & set(prop2.keys()) 
        for key in propKeyInters:
            propMerged[key] = merge_data_types(prop1[key], prop2[key]) # merged value
            
        # prop1 property keys not in the intersection:
        prop1KeyOther = set(prop1.keys()) - propKeyInters 
        for key in prop1KeyOther:
            # Note: this key-value pair is optional
            prop1other = prop1[key]
            if type(prop1other) == str and "?" not in prop1other:
                prop1other += " ?"
            elif type(prop1other) == list and "?" not in prop1other[0]:
                #prop1other.append("?")
                prop1other= [str(prop1other[0]) + " ?"]
            elif type(prop1other) == dict:
                prop1other["meta_mandatory"] = False
            propMerged[key] = prop1other # update output
            
        # prop2 property keys not in the intersection:
        prop2KeyOther = set(prop2.keys()) - propKeyInters 
        for key in prop2KeyOther:
            # Note: this key-value pair is optional
            prop2other = prop2[key]
            if type(prop2other) == str and "?" not in prop2other:
                prop2other += " ?"
            elif type(prop2other) == list and "?" not in prop2other[0]:
                #prop2other.append("?")
                prop2other= [str(prop2other[0]) + " ?"]
            elif type(prop2other) == dict:
                prop2other["meta_mandatory"] = False    
            propMerged[key] = prop2other # update output
            
        #propMerged = json.dumps(propMerged)
        propMerged = propMerged
           
            
    elif type(prop1) == str:
        # prop2 is also a string and the merged data type is a string
        
        # to be able to deal with optional string data types
        optional = False
        if "?" in prop1 or "?" in prop2:
            optional = True
            prop1 = prop1.replace(" ?",'')
            prop2 = prop2.replace(" ?",'')
            if prop1 == prop2:
                propMerged = prop1 + " ?"
                return propMerged
            
        if "+" in prop1 or "+" in prop2:
            return " + ".join([prop1, prop2])
        
        prop1dicts = [] # list of dictionnaries contained in prop1
        prop2dicts = [] # list of dictionnaries contained in prop2
        
        ## deal with potential dicts
        prop1dicts, prop1elems = get_dict_content(prop1)
        prop2dicts, prop2elems = get_dict_content(prop2)
        if prop1dicts and prop2dicts:
            # both prop1lists and prop2lists are non-empty (i.e., they contain lists)
            propdictList = []
            for pair in list(itertools.product(prop1dicts, prop2dicts)):
                mergedPair = merge_data_types(pair[0], pair[1]) # recursive call
                propdictList.append(mergedPair)
            sep = " + " # separator for the union of data types
            propMerged = " + ".join([" + ".join(set(prop1elems) | set(prop2elems)), str([" + ".join(propdictList)])])
            
        else:
            # at least one of the two is empty
            if prop1dicts:
                # prop1list is not empty
                prop1elems.append(str(prop1dicts)) 
            elif prop2dicts:
                # prop2list is not empty
                prop2elems.append(str(prop2dicts))
                    
            propMerged = " + ".join(set(prop1elems) | set(prop2elems))
            

        ## deal with potential lists          
        prop1lists, prop1elems = get_list_content(prop1)
        prop2lists, prop2elems = get_list_content(prop2)   
        
        if prop1lists and prop2lists:
            # both prop1lists and prop2lists are non-empty (i.e., they contain lists)
            proplistSet = set()
            for pair in list(itertools.product(prop1lists, prop2lists)):
                mergedPair = merge_data_types(pair[0], pair[1]) # recursive call
                proplistSet.update(set(mergedPair.split(" + ")))
            sep = " + " # separator for the union of data types
            if not prop1elems and not prop2elems:
                # both are empty
                propMerged = [" + ".join(proplistSet)]
                ## deal with optional string data type
                if optional:
                    propMerged = str([propMerged[0].replace(" ?",'') + " ?"])
            else:
                # one is non-empty
                propMerged = " + ".join([" + ".join(set(prop1elems) | set(prop2elems)), str([" + ".join(proplistSet)])])
            
        else:
            # at least one of the two is empty
            if prop1lists:
                # prop1list is not empty
                prop1elems.append(str(prop1lists)) 
            elif prop2lists:
                # prop2list is not empty
                prop2elems.append(str(prop2lists))
            
            propMerged = " + ".join(set(prop1elems) | set(prop2elems))
            
            ## deal with optional string data type
            if optional:
                propMerged = propMerged.replace(" ?",'') + " ?"
            
            
    elif type(prop1) == bool:
        propMerged = " + ".join({str(prop1), str(prop2)})
            
    else:
        print("{} is not a recognized data type. The merged data type is set as 'Null'.".format(type(prop1)))
        propMerged = "Null"
        
    return propMerged


def types_intersections(tab):
    """ Get the pairwise intersections of all types (in terms of property keys)
    in the list tab and the list of types that do not intersect any of the other types.
    
    Parameters
    ----------
    tab : Python list of dicts
        The list of types to intersect. Types are stored in dicts.
    
    Returns
    -------
    inters : Python list of sets
        The list of nonempty pairwise intersections.
    intersNo : Python list of sets
        The list of sets that do not intersect with any other set.
    intersPairs : Python list of sets
        The list of pairs that intersect.
        
    """
    
    inters = [] # list of pairwise property keys intersections
    intersYes = [] # list of sets that intersect with at least one other set
    intersNo = [] # list of sets that do not intersect with any other set
    intersPairs = [] # list of pairs that intersect (there cannot be duplicates)
    intersPropsList = [] # list of properties s.t. their property keys pairwise intersect
    
    groupedbyx=[]

    for i in range(len(tab)):
        
        x = tab[i]
        xkeys = set([k for k in x.keys() if not re.search("meta", k)]) # property keys of the type x (all "meta" properties are not taken into account)
        noInters_x = True # boolean to identify sets with empty intersections with every other sets
        
        xinters=[x]
        for j in range(len(tab[i:])-1):
            y = tab[j+i+1]
            ykeys = set([k for k in y.keys() if not re.search("meta", k)]) # property keys of the type y
            xintersy = xkeys & ykeys # intersection between x and y property keys
            
            if xintersy != set():
                # the intersection is not empty
                noInters_x = False
                
                # append the list of intersections
                if xintersy not in inters:
                    inters.append(xintersy)
                    intersProps = {}
                    for key in xintersy:
                        intersProps[key]= merge_data_types(x[key],y[key]) #x[key]
                    intersPropsList.append(intersProps)
                xinters.append(y)
                
                # append the list of intersecting sets
                intersYes.append(x)
                intersYes.append(y)
                
                # append the list of pairs
                intersPairs.append((x,y))
            
            
        groupedbyx.append(xinters)
        if noInters_x and x not in intersYes:
            # for every y, x & y = empty set
            # Note: there cannot be duplicates
            intersNo.append(x)
            
            
    ## remove duplicates  
    # convert to list of lists
    intersLists = list(map(list, inters))  
    # sort
    intersLists.sort() 
    # remove duplicates, convert to list of sets
    inters = list(map(set, list(elem for elem,_ in itertools.groupby(intersLists)))) 
    
    return intersPropsList


def get_labels(props):
    """ get labels from dict of property types props """
    labels = set() # set of labels
    for key, value in props.items():
        if value == "Void":
            labels.add(key)
    if not labels:
        # there are no labels in the set of property types
        labels = set()#set(props.keys())
    return labels


def crt_inheritance_edge_unlabeled(elem0, elem1, edges, nodes):
    """ Procedure that creates the inheritance edge between two nodes of properties
        elem0 and elem1, if it applies and appends the nodes and edges list accordingly.
        Labels-as-properties and labels-only paradigms are implemented. If their result
        conflicts, the user is asked to choose.
    
    Parameters
    ----------
    elem0 : dict
        The properties of the first node.
    elem1 : dict
        The properties of the second node.
    edges : Numpy array
        List of edges stored with the following format:
        (source labels, target labels, {"type": edge labels, "property key": property value, ...})
    nodes : Numpy array
        List of nodes stored with the following format:
        (labels, {"property key":property value, ...})
     
    """
    lab0, lab1 = get_labels(elem0), get_labels(elem1) # nodes label sets
    
    if elem0 != elem1:
        # the nodes have different properties
        
        ### properties paradigm
    
        if set(elem1.keys()).issubset(set(elem0.keys())):      
            # elem0 is a subtype of elem1
            subprops, supprops = elem0, elem1 
            # to deal with multi-labeled node types:
            # labels of the subtype and supertype, respectively
            lab0, lab1 = get_labels(elem0), get_labels(elem1) # get labels
            sublabprop, superlabprop = label_format(lab0), label_format(lab1)
            
            # create the corresponding inheritance edges and append edge list
            edges[str(elem0['meta_id']) + "::SubtypeOf::" + str(elem1['meta_id'])]={}
 
            
        elif set(elem0.keys()).issubset(set(elem1.keys())):
            # elem1 is a subtype of elem0
            subprops, supprops = elem1, elem0
            # to deal with multi-labeled node types:
            # labels of the subtype and supertype, respectively
            lab0, lab1 = get_labels(elem0), get_labels(elem1) # get labels
            sublabprop, superlabprop = label_format(lab1), label_format(lab0)
                        
            # create the corresponding inheritance edges and append edge list
            edges[str(elem1['meta_id']) + "::SubtypeOf::" + str(elem0['meta_id'])]={}

            
        else:
            # elem0 and elem1 are not extended from one another
            sublabprop, superlabprop = "False", "False" #"", ""
            
        if sublabprop == "":
                sublabprop = str(subprops['meta_id'])
        if superlabprop == "":
            superlabprop = str(supprops['meta_id'])          
            
            
            


def infer_unlabeled_node_hierarchies(schema, filename):
    """ infers node hierarchies in the provided schema
        and writes it in a JSON file. 
        
    
    Parameters
    ----------
    schema : dict
        Schema from which to infer node hierarchies.
    filename : str
        Name of the output file containing the schema in JSON format.
        
    Return
    ------
    nodes : dict
        Contains the nodes of the schema.
    edges : dict
        Contains the edges of the schema.
    
    """
    ### Outputs 
    edges = copy.deepcopy(schema['Edges'])
    nodes = copy.deepcopy(schema['Nodes'])
    
    # nodes property keys
    propKeys = [] # list of property keys (that are not meta properties)
    for elem in nodes:
        if set(elem.keys()) != set():
            propKeys.append(set([x for x in list(elem.keys()) if 'meta_' not in x]))
    
    ### find supertypes    
    supertypes = types_intersections(nodes)
    
    ## append node list with supertypes
    i = -1 # counter to id the newly identified supertypes
    for stype in supertypes:
        # append node list with newly discovered supertypes
        if stype.keys() not in propKeys:
            stype['meta_id'] = i
            i -= 1
            nodes.append(stype)
    
                  
    ### find subtypes (including overlapping subtypes) and deal with them
    for i in range(len(nodes)):
        elem0 = nodes[i]
                            
        for j in range(len(nodes[i:])):
            elem1 = nodes[j+i]
            if elem0 != {} and elem1 != {}:
                # create inheritance edge if it applies
                crt_inheritance_edge_unlabeled(elem0, elem1, edges, nodes)
    
    ### write output file
    out = open(filename,'w')
    #out.write("{nodes:" + str(nodes) + "},\n edges:" + str(edges) +"}") 
    # create the schema dict
    schema = {}
    schema['nodes'] = nodes
    schema['edges'] = edges
    # dump the schema to json and write it in the output file
    out.write(json.dumps(schema))
    out.close()
    return nodes, edges



###### create Neo4j schema graph
def create_Neo4j_pgschema_unlabeled(Nodes, Edges, driver, all_edges=True):
    """ creates a Neo4j schema graph from the list of nodes and edges.
    
    Parameters
    ----------
    nodes : dict
        Contains the nodes of the schema
    edges : dict
        Contains the edges of the schema
    driver : GraphDatabase.driver object
        Driver used to access the location where the Neo4j schema will be stored.
    all_edges : Bool, optional
        True by default. 
        False to remove superfluous edges in cases of multi-layered hierarchies. 
        (the schema will be less cluttered but more difficult to query) 
    
    """
    nodes = copy.deepcopy(Nodes)
    edges = copy.deepcopy(Edges)
  
    ### format the nodes and edges to convert to Neo4j graph (using ReGraph)
           
    ## nodes property keys with their meta_id
    propKeys = [] # list of property keys (that are not meta properties)
    for elem in nodes:
        if set(elem.keys()) != set():
            propKeys.append([set([x for x in list(elem.keys()) if 'meta_' not in x])] + [elem['meta_id']])
    
    ## schema edges
    schemaEdges = [] 
    for keyVal, value in edges.items():
        unlabSource = False # to keep track if this edge's source node is unlabeled
        unlabTarget = False # to keep track if this edge's target node is unlabeled
        for key, prop in value.items():
            if key == 'meta_source_unlabeled':
                # the source node is unlabeled
                unlabSource = True
            if key == 'meta_target_unlabeled':
                # the target node is unlabeled
                unlabTarget = True
            
            if type(prop) == dict:
                prop = str(json.dumps(prop))
            value[key] = str(prop)
            
        source, etype, target = keyVal.lstrip(":").split("::")
        value['type']=etype
        
        # map the unlabeled source/target nodes to their node type
        if unlabSource:
            for nkeys in propKeys:
                if set(str(source).split(":")) == nkeys[0]:
                    source = nkeys[1]       
        if unlabTarget:
            for nkeys in propKeys:
                if set(str(target).split(":")) == nkeys[0]:
                    target = nkeys[1]
        
        # append edge list in ReGraph-compatible format
        schemaEdges.append((source, target, value))
        
    
    ## schema nodes
    schemaNodes = []
    for ntype in nodes:
        labels = get_labels(ntype) # get node type labels
        if ntype != {}:
            if "id" in ntype.keys(): 
                ntype['ID'] = ntype.pop("id")

            # property values converted to string     
            for keyVal, value in ntype.items():
                ntype[keyVal] = str(ntype[keyVal])
            
        if labels == set():
            # unlabeled node type
            lab = ntype['meta_id']
        else:
            lab = label_format(labels)
        
        # append node list in ReGraph-compatible format
        ntype["Label"]=lab
        schemaNodes.append((ntype['meta_id'],ntype))
        
    
    ### create the Neo4j graph
    schemaNeo4j = Neo4jGraph(driver=driver)
    schemaNeo4j._clear()
    schemaNeo4j.add_nodes_from(schemaNodes)
    schemaNeo4j.add_edges_from(schemaEdges)
    

    ### remove superfluous edges
    if not all_edges:
        with driver.session() as session:
            session.run("MATCH p=(n)-[s:edge{type:['SubtypeOf']}]->(m)\
                        -[r:edge*1.. {type:['SubtypeOf']}]->(o), \
                        q = (n)-[t:edge{type:['SubtypeOf']}]->(o) \
                        DELETE t")
        
