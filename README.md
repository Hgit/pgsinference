# SchemI - Schema Inference for Property Graphs (PG)

## About the Project
This end-to-end Python pipeline infers PG schemas from input PG stored in Neo4j. Our method leverages a MapReduce type inference method (by [Baazizi et al.](https://doi.org/10.1007/s00778-018-0532-7)) and introduces a node hierarchy inference technique to output PG schemas that handle complex and nested property values, multi-labeled nodes, node hierarchies and overlapping node types, in addition to containing information about edge cardinality constraints and optionality of properties. We proposed two implementations: a *labels-oriented* and a *properties-oriented* variant.

It was developed by Hanâ Lbath in the context of her Master's thesis in Computer Science (at ENS Lyon)/Bioinformatics and Modeling (at INSA Lyon) under the supervision of Angela Bonifati and Russ Harmer.

## Code
The code is organized as follows:
 - *labels-oriented* variant: run **SchemI\_labels\_oriented.py** to infer a PG schema. The functions called in this script are stored in the **labelsOriented** folder.

 - *properties-oriented* variant: run **SchemI\_properties\_oriented.py** to infer a PG schema. The functions called in this script are stored in the **propertiesOriented** folder

 - the **MapReduce** folder contain the binaries of the MapReduce type inference algorithm.
